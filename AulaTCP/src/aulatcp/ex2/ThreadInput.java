/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aulatcp.ex2;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**A class to represent a input thread. 
 * This kind of thread is used to receive messages in server applications.
 * 
 * @author Paulo Batista
 */
public class ThreadInput extends Thread {
    private Socket socket;
    private String origin;
    
    public ThreadInput(Socket socket, String origin) {
        this.socket = socket;
        this.origin = origin;
    }
    
    @Override
    public void run() {
        while(true) {
            DataInputStream in;
            String buffer = "";
            try {
                in = new DataInputStream(this.socket.getInputStream());
                buffer = in.readUTF();
                System.out.println(this.origin + ": " + buffer);
                if (buffer.equals("SAIR")) {
                    break;
                }
            } catch (IOException ex) {
                Logger.getLogger(ThreadInput.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
        }
    }
}
