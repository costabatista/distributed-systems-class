/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aulatcp.ex2;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**A class to represent a server to be used in TCP connection by Threads.
 * Description: It Receive a connection, create Thread, receive a message and finishes a connection.
 * @author Paulo Batista -- pauloc@linuxmail.org
 * @version 1.0
 *
 * @author Paulo Batista
 */
public class TCPServer {
    public static void main(String args[]) {
        try{
            int port = 6666;
            ServerSocket serverSocket = new ServerSocket(port);
            Socket socket = serverSocket.accept();
            ThreadInput ti = new ThreadInput(socket, "Client");
            ThreadOutput to = new ThreadOutput(socket);
            
            ti.setDaemon(true);
            to.setDaemon(true);
            
            ti.start();
            to.start();
            
            while (ti.isAlive() && to.isAlive()) {
                Thread.sleep(2000);
            }
        } catch (IOException ex) {
            Logger.getLogger(TCPServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(TCPServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
}


