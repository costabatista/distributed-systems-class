/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aulatcp.ex2;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**A class to create a client that uses tcp connection.
 * Description: Send an information to server and receive messages from it. This implementation uses a example made in class.
 * If client sends "PARAR" the connection is finished.
 * @version 1.0
 *
 * @author Paulo Baitsta
 */
public class TCPClient {

    public static void main(String args[]) {
        Socket socket = null;

        try {
            int port = 6666;
            InetAddress serverAddr = InetAddress.getByName("127.0.0.1");
            socket = new Socket(serverAddr, port);

            ThreadInput ti = new ThreadInput(socket, "Server");
            ThreadOutput to = new ThreadOutput(socket);

            ti.setDaemon(true);
            to.setDaemon(true);

            ti.start();
            to.start();
            
            while (ti.isAlive() && to.isAlive()) {
                Thread.sleep(2000);
            }
        } catch (UnknownHostException ex) {
            Logger.getLogger(TCPClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TCPClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(TCPClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
