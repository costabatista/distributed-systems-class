/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aulatcp.ex2;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/** A class to represent a thread that send messages.
 *
 * @author costa
 */
public class ThreadOutput extends Thread {
    private Socket socket;
    
    
    public ThreadOutput(Socket socket) {
        this.socket = socket;
        
    }
    
    @Override
    public void run() {
        Scanner scan = new Scanner(System.in);
        while(true) {
            try {
                DataOutputStream out = new DataOutputStream(this.socket.getOutputStream());
                
                String buffer = scan.nextLine();
                out.writeUTF(buffer);
                
                if (buffer.equals("SAIR")) {
                    break;
                }
            } catch (IOException ex) {
                Logger.getLogger(ThreadOutput.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
