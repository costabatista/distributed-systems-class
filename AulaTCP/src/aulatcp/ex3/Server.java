/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aulatcp.ex3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/** Class Server
 * A class to represent a server. it extends Thread.
 *
 * @author Paulo Batista
 */
public class Server extends Thread{
    
    private Socket socket;
    private InputStream inputStream;
    private InputStreamReader inputStreamReader;
    private BufferedReader bufferedReader;
    private static ArrayList<BufferedWriter> clients;
    
    
    public Server(Socket socket) {
        this.socket = socket;
        
        try {
            this.inputStream = socket.getInputStream();
            this.inputStreamReader = new InputStreamReader(this.inputStream);
            this.bufferedReader = new BufferedReader(this.inputStreamReader);
        } catch(IOException ioe) {
            System.out.println(ioe.getLocalizedMessage());
        }
    }
    
    /** sendMessageToAllClients
     * A method to send a message to client list. The message will be send to all clients except the sender.
     * @param bufferedWriter a buffer to write a message to all clients. 
     * @param message the message to be send to all clients.
     */
    private void sendMessageToAllClients(BufferedWriter bufferedWriter,String message) {
        for (BufferedWriter buffw : clients) {
            if (buffw != bufferedWriter) {
                try {
                    buffw.write(message + "\n");
                    buffw.flush();
                } catch(IOException ioe) {
                    System.out.println(ioe.getLocalizedMessage());
                }
            }
        }
    }
    
    @Override
    public void run() {
        String message;
        try{
            OutputStream outputStream = this.socket.getOutputStream();
            Writer writer = new OutputStreamWriter(outputStream);
            BufferedWriter bufferedWriter = new BufferedWriter(writer);
            clients.add(bufferedWriter);
            
            while(true) {
                message = this.bufferedReader.readLine();
                if (message.equals("QUIT")) {
                    clients.remove(bufferedWriter);
                    break;
                }
                
                this.sendMessageToAllClients(bufferedWriter, message);
            }
        } catch(IOException ioe) {
            System.out.println(ioe.getLocalizedMessage());
        }
    }
    
    public static void main (String args[]) {
        int port = 6666;
        clients = new ArrayList<>();
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            while(true) {
                System.out.println("-- Waiting for conection in port 6666 --");
                Socket socket = serverSocket.accept();
                System.out.println("-- New client connected --");
                Server newServer = new Server(socket);
                newServer.start();
            }
            
        } catch(IOException ioe) {
            System.out.println(ioe.getLocalizedMessage());
        }
    }
}
