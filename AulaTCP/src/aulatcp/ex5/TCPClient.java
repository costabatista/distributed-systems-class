/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aulatcp.ex5;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author costa
 */
public class TCPClient {

    private final Socket socket;

    public TCPClient(Socket socket) {
        this.socket = socket;
    }
    /**
     * A method to listen all that is received from socket connection
     */
    public void listenning() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                String incommingMessage = "";
                try {
                    InputStream input = socket.getInputStream();
                    DataInputStream in = new DataInputStream(input);
                    while (true) {
                        incommingMessage = in.readUTF();
                        if (incommingMessage.equals("exit")) {
                            System.out.println("Finishing connection");
                        } else {
                            System.out.println("Message in: " + incommingMessage);
                        }
                    }
                } catch (IOException ex) {
                    Logger.getLogger(TCPClient.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });
        thread.start();
    }
    /**
     * A method to send any string that a person writes in console. Sadly scan is a operation
     * that block the application flux.
     * @throws IOException 
     */
    public void sending() throws IOException {
        Scanner scan = new Scanner(System.in);
        String outMessage = "";
        OutputStream output = this.socket.getOutputStream();
        DataOutputStream out = new DataOutputStream(output);
        while(true) {
            outMessage = scan.nextLine();
            if(outMessage.equals("exit")) {
                out.writeUTF("exit");
                out.flush();
                break;
            }
            else {
                out.writeUTF(outMessage);
                out.flush();
            }
        }
    }
    public static void main(String args[]) {
        int port = 6666;
        try {
            InetAddress serverAddr = InetAddress.getByName("127.0.0.1");
            Socket socket = new Socket(serverAddr, port);
            TCPClient client = new TCPClient(socket);
            client.listenning();
            client.sending();

        } catch (UnknownHostException uhe) {
            System.out.println(uhe.getMessage());
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }

    }
}
