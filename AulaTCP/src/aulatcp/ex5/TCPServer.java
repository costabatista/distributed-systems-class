/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aulatcp.ex5;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author costa
 */
public class TCPServer extends Thread {
   
    private final PipedInputStream pipeIn;
    private final PipedOutputStream pipeOut;
    private final Socket socket;
    private boolean isInterrupted;
    
    /**
     * A constructor that initializes a tcp server instance
     * @param socket a socket to connect a instance of a server and a client
     * @param pipeIn a pipe that will receive a stream of bytes
     * @param pipeOut a pipe that will send a stream of bytes.
     */
    public TCPServer(Socket socket, PipedInputStream pipeIn, PipedOutputStream pipeOut) {
        this.pipeIn = pipeIn;
        this.pipeOut = pipeOut;
        this.socket = socket;
        this.isInterrupted = false;
    }
    
    /**
     * A method to exit the main application server
     */
    private void exit() {
        this.isInterrupted = true;
    }
    @Override
    public void run() {
        try {
            InputStream input = this.socket.getInputStream();
            DataInputStream in = new DataInputStream(input);
            OutputStream output = this.socket.getOutputStream();
            DataOutputStream out = new DataOutputStream(output);
            String inputMessage = "";
            while (!this.isInterrupted) {
                inputMessage = in.readUTF();
                if(inputMessage.equals("exit")) {
                    this.exit();
                } else {
                    this.pipeOut.write(inputMessage.getBytes());
                    int data;
                    String outMessage = ""; 
                    
                    while(this.pipeIn.available() > 0) {
                        data = this.pipeIn.read();
                        outMessage = outMessage + (char) data;
                    }
                    //this.pipeOut.write(outMessage.getBytes());
                    System.out.println(outMessage);
                    out.writeUTF(outMessage);
                    out.flush();
                    outMessage = "";
                }
            }
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }
    }

    public static void main(String args[]) {
        int port = 6666;
        try {
            PipedOutputStream outOne = new PipedOutputStream();
            PipedInputStream inOne = new PipedInputStream();
            PipedOutputStream outTwo = new PipedOutputStream();
            PipedInputStream inTwo = new PipedInputStream();
            inOne.connect(outOne);
            inTwo.connect(outTwo);
            

            ServerSocket serverSocket = new ServerSocket(port);
            Socket socketOne = serverSocket.accept();

            TCPServer serverOne = new TCPServer(socketOne, inOne, outTwo);

            Socket socketTwo = serverSocket.accept();
            TCPServer serverTwo = new TCPServer(socketTwo, inTwo, outOne);

            serverOne.start();
            serverTwo.start();

        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }
    }
}
