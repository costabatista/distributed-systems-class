/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aulatcp.ex4;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 *
 * @author costa
 */
public class TCPClient {

    public static void main(String args[]) {
        int port = 6666;
        String standardDownloadDirectory = System.getProperty("user.dir") + File.separator + "standard"
                + File.separator + "download";
        try {
            InetAddress serverAddr = InetAddress.getByName("127.0.0.1");
            Socket socket = new Socket(serverAddr, port);
            OutputStream outputStream = socket.getOutputStream();
            DataOutputStream out = new DataOutputStream(outputStream);
            InputStream inputStream = socket.getInputStream();
            DataInputStream in = new DataInputStream(inputStream);
            Scanner scan = new Scanner(System.in);
            String request = "";

            while (true) {
                System.out.print("command: ");
                request = scan.nextLine();

                out.writeUTF(request);
                out.flush();
                if (request.equals("times")) {
                    String times = in.readUTF();
                    System.out.println(times);
                } else if (request.equals("date")) {
                    String date = in.readUTF();
                    System.out.println(date);
                } else if (request.equals("files")) {
                    String files = in.readUTF();
                    System.out.println(files);
                } else if (request.contains("down")) {
                    String fileName = request.split(" ")[1];
                    
                    byte b = in.readByte();
                    if (b == -1) {
                        System.out.println("There isn't any file named as " + fileName);
                    } else {
                        FileOutputStream fileOutputStream = new FileOutputStream(standardDownloadDirectory + File.separator
                            + fileName);
                        while (b != -1) {
                            fileOutputStream.write(b);
                            fileOutputStream.flush();
                            b = in.readByte();
                        }

                        fileOutputStream.close();

                        int numberOfBytes = in.readInt();

                        System.out.println("The file " + fileName + " is downloaded. " + numberOfBytes + " bytes  of file.");

                    }

                } else if (request.equals("exit")) {
                    String finish = in.readUTF();
                    System.out.println(finish);
                    outputStream.close();
                    inputStream.close();
                    out.close();
                    in.close();
                    socket.close();
                    break;
                }

            }

        } catch (UnknownHostException uhe) {
            System.out.println(uhe.getMessage());
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }

    }
}
