/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aulatcp.ex4;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author costa
 */
public class TCPServer extends Thread {

    private final Socket socket;
    private String standardFolder;
    private InputStream inputStream;
    private DataInputStream in;
    private boolean isInterrupted;
    /**
     * The construction that initializes a socket in tcp server class
     * @param socket a socket that is received from main method.
     */
    public TCPServer(Socket socket) {
        this.socket = socket;
        this.standardFolder = this.standardFolder = System.getProperty("user.dir") + File.separator + "standard"
                + File.separator + "files";

        try {
            this.inputStream = this.socket.getInputStream();
            this.in = new DataInputStream(this.inputStream);
            this.isInterrupted = false;
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }
    }
    /**
     * A method to get the current system time and send to client service that is connected to
     * server through a socket connection
     * @throws IOException 
     */
    private void getCurrentSystemTime() throws IOException {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        String currentSystemTime = simpleDateFormat.format(calendar.getTime());

        OutputStream outputStream = this.socket.getOutputStream();
        DataOutputStream out = new DataOutputStream(outputStream);
        out.writeUTF(currentSystemTime);
        out.flush();

    }
    
    /**
     * A method to get the current system date and send it to client service connected through
     * a socket connection
     * @throws IOException 
     */
    private void getCurrentSystemDate() throws IOException {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/YYYY");
        String currentSystemDate = simpleDateFormat.format(calendar.getTime());

        OutputStream outputStream = this.socket.getOutputStream();
        DataOutputStream out = new DataOutputStream(outputStream);
        out.writeUTF(currentSystemDate);
        out.flush();
    }

    /**
     * A method to exit the main server process. It also sends a message to finish client instance.
     * @throws IOException 
     */
    private void exit() throws IOException {
        Thread.currentThread().interrupt();
        if (Thread.currentThread().isInterrupted()) {
            OutputStream outputStream = this.socket.getOutputStream();
            DataOutputStream out = new DataOutputStream(outputStream);
            out.writeUTF("Connection finished.");
            out.flush();
            out.close();
            outputStream.close();
            this.inputStream.close();
            this.socket.close();
            this.isInterrupted = true;
        }
    }
    
    /**
     * A method to get the list file that is placed in a predefined standard directory. The data is
     * sent to client service connected through a socket connection.
     * @throws IOException 
     */
    private void getFilesFromStandardDirectory() throws IOException {
        File directoryFile = new File(this.standardFolder);
        String files = "";
        for (File f : directoryFile.listFiles()) {
            files = files + f.getName() + "\n";
        }
        files = files.trim();
        int numberOfFiles = directoryFile.listFiles().length;
        if (numberOfFiles == 0) {
            files = "The standard directory is empty.";
        } else {
            files = "Number of files in standard directory: " + numberOfFiles + ",\n" + files;
        }
        OutputStream outputStream = this.socket.getOutputStream();
        DataOutputStream out = new DataOutputStream(outputStream);
        
        out.writeUTF(files);
        out.flush();
        
    }

    /**
     * A method to download a file that is placed in standard directory
     * @param name the name of requested file
     * @throws IOException 
     */
    private void downloadFile(String name) throws IOException {
       String path = this.standardFolder;
       File file = new File(path + File.separator + name);
       if(file.exists()) {
           byte[] getBytes = new byte[(int) file.length()];
           InputStream inputStream = new FileInputStream(file);
           inputStream.read(getBytes);
           inputStream.close();
          
           
           OutputStream outputStream = this.socket.getOutputStream();
           DataOutputStream out = new DataOutputStream(outputStream);
           for(byte b : getBytes) {
               out.write(b);
               out.flush();
           }
           out.write(-1);
           out.flush();
           
           out.writeInt(getBytes.length);
           out.flush();
       }
       else {
           OutputStream outputStream = this.socket.getOutputStream();
           DataOutputStream out = new DataOutputStream(outputStream);
           out.write(-1);
           out.flush();
       }
    }
    @Override
    public void run() {

        String request = "";

        while (!this.isInterrupted) {
            try {
                request = this.in.readUTF();
                if (request.equals("times")) {
                    this.getCurrentSystemTime();
                } else if (request.equals("date")) {
                    this.getCurrentSystemDate();
                } else if (request.equals("files")) {
                    this.getFilesFromStandardDirectory();
                } else if(request.contains("down")) {
                    String fileName = request.split(" ")[1];
                    this.downloadFile(fileName);
                }else if (request.equals("exit")) {
                    this.exit();
                    this.in.close();

                }
            } catch (IOException ioe) {
                System.out.println(ioe.getMessage());
            }
        }
    }

    public static void main(String args[]) {
        int port = 6666;

        try {
            ServerSocket serverSocket = new ServerSocket(port);

            while (true) {
                Socket socket = serverSocket.accept();

                TCPServer server = new TCPServer(socket);

                server.start();
            }

        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }

    }
}
