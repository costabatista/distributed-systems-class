/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jserial.service;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import jserial.service.client.Commitment;

/**
 *
 * @author costa
 */
public class Client {

    private final Socket socket;
    private final DataInputStream in;
    private final DataOutputStream out;
    private final String directory;

    public Client(Socket socket) throws IOException {
        this.socket = socket;

        InputStream input = this.socket.getInputStream();
        OutputStream output = this.socket.getOutputStream();

        this.in = new DataInputStream(input);
        this.out = new DataOutputStream(output);

        this.directory = "serialClient";
    }
    
    /**
     * A method to serialize a commitment 
     * @param commitment A commitment entity to be serialized
     * @throws IOException 
     */
    private void serializeCommitment(Commitment commitment) throws IOException {
        String filePath = this.directory + File.separator + commitment.getIdClient() + ".ser";
        FileOutputStream fileOut = new FileOutputStream(filePath);

        ObjectOutputStream objOut = new ObjectOutputStream(fileOut);
        objOut.writeObject(commitment);
        objOut.flush();

        objOut.close();
        fileOut.close();
    }
    
    /**
     * A method to send a serialized commitment to the respective server
     * @param commitment an object that is not serialized that will send to server. 
     * @throws IOException 
     */
    private void sendSerializedCommitment(Commitment commitment) throws IOException {
        String filePath = this.directory + File.separator + commitment.getIdClient() + ".ser";
        File file = new File(filePath);
        FileInputStream fileIn = new FileInputStream(file);

        byte[] fileByte = new byte[(int) file.length()];

        fileIn.read(fileByte);

        for (byte b : fileByte) {
            this.out.write(b);
            this.out.flush();
        }

        this.out.write(-1);
        this.out.flush();

        fileIn.close();
    }

    /**
     * A method to serialize a list of commitments
     * @param clientId a client id to retrieve commitments.
     * @return a file that contains a serialized list of commitments.
     * @throws IOException 
     */
    private File getSerializedCommitmentList(String clientId) throws IOException {
        byte received = this.in.readByte();
        String filePath = this.directory + File.separator + clientId + "_commitments.ser";

        FileOutputStream fileOut = new FileOutputStream(filePath);

        while (received != -1) {
            fileOut.write(received);
            fileOut.flush();
            received = this.in.readByte();
        }

        fileOut.close();

        File file = new File(filePath);
        return file;
    }
    
    /**
     * A method to retrieve the data from a file that contains serialized commitment list
     * @param file the file that contains serialized data about commitment list
     * @return return a non serialized commitment list
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    private List<Commitment> getDeserializedCommitmentList(File file) throws IOException, ClassNotFoundException {
        FileInputStream fileIn = new FileInputStream(file);
        ObjectInputStream objectIn = new ObjectInputStream(fileIn);

        ArrayList<Commitment> commitments = (ArrayList<Commitment>) objectIn.readObject();

        objectIn.close();
        fileIn.close();

        return commitments;
    }

    /**
     * A method to print a commitment in terminal
     * @param commitment a commitment to be printed.
     */
    private void printCommitment(Commitment commitment) {
        System.out.println("----------------------------");
        System.out.println("client id: " + commitment.getIdClient());
        System.out.println("description: " + commitment.getDescription());
        System.out.println("date: " + commitment.getDate());
        System.out.println("hour: " + commitment.getHour());
        System.out.println("----------------------------");
    }

    /**
     * A method to stablish a communication between client and server
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    public void communicate() throws IOException, ClassNotFoundException {
        String request;
        Scanner scanner = new Scanner(System.in);
        while (true) {
            request = scanner.nextLine();
            if (request.startsWith("add commitment")) {
                this.out.writeUTF(request);
                Commitment commitment = new Commitment();

                String clientId = request.split(" ")[2];
                commitment.setIdClient(new Integer(clientId));

                System.out.print("Description: ");
                String description = scanner.nextLine();
                commitment.setDescription(description);

                System.out.print("Date: ");
                String date = scanner.nextLine();
                commitment.setDate(date);

                System.out.print("Hour: ");
                String hour = scanner.nextLine();
                commitment.setHour(hour);

                this.serializeCommitment(commitment);
                this.sendSerializedCommitment(commitment);
            } else if (request.startsWith("get commitments")) {
                this.out.writeUTF(request);
                String clientId = request.split(" ")[2];
                File serializedFile = this.getSerializedCommitmentList(clientId);
                List<Commitment> commitments = this.getDeserializedCommitmentList(serializedFile);
                for(Commitment c : commitments) {
                    this.printCommitment(c);
                }
            } else if (request.equals("exit")) {
                this.out.writeUTF("exit");
                this.out.flush();
                this.socket.close();
                
                System.out.println("Exiting client application");
                break;
            }
        }
    }

    public static void main(String args[]) {
        int port = 6668;
        try {
            InetAddress serverAddr = InetAddress.getByName("127.0.0.1");
            Socket socket = new Socket(serverAddr, port);

            Client client = new Client(socket);
            client.communicate();
        } catch (UnknownHostException uhe) {
            System.out.println(uhe.getMessage());
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        } catch (ClassNotFoundException cnfe) {
            System.out.println(cnfe.getMessage());
        }
    }
}
