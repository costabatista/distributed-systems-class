/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jserial.service;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import jserial.service.client.Commitment;

/**
 *
 * @author costa
 */
public class Server extends Thread {

    private final Socket socket;
    private final DataOutputStream out;
    private final DataInputStream in;
    private static List<Commitment> commitments;
    private final String directory;

    public Server(Socket socket) throws IOException {
        this.socket = socket;

        InputStream input = this.socket.getInputStream();
        this.in = new DataInputStream(input);

        OutputStream output = this.socket.getOutputStream();
        this.out = new DataOutputStream(output);

        this.directory = "serialServer";
    }

    private File getSerializedFile(String filePath) throws IOException {
        byte received = this.in.readByte();

        FileOutputStream fileOut = new FileOutputStream(filePath);

        while (received != -1) {
            fileOut.write(received);
            fileOut.flush();
            received = this.in.readByte();
        }

        fileOut.close();

        File serializedFile = new File(filePath);

        return serializedFile;
    }

    private Object deserializeCommitmentFile(File serializedFile) throws IOException, ClassNotFoundException {
        FileInputStream fileIn = new FileInputStream(serializedFile);
        ObjectInputStream objectInput = new ObjectInputStream(fileIn);
        Object object = objectInput.readObject();

        objectInput.close();
        fileIn.close();

        return object;
    }

    private void printCommitment(Commitment commitment) {
        System.out.println("\n\n----------------------------");
        System.out.println("client id: " + commitment.getIdClient());
        System.out.println("description: " + commitment.getDescription());
        System.out.println("date: " + commitment.getDate());
        System.out.println("hour: " + commitment.getHour());
        System.out.println("----------------------------\n\n");
    }

    private void addCommitment(String idClient) throws IOException, ClassNotFoundException {
        String filePath = this.directory + File.separator + idClient + ".ser";

        File serializedFile = this.getSerializedFile(filePath);

        Commitment commitment = (Commitment) this.deserializeCommitmentFile(serializedFile);

        commitments.add(commitment);

        this.printCommitment(commitment);
    }

    private void serializeCommitmentList(int clientId) throws IOException {
        List<Commitment> selectedCommitments = new ArrayList<>();

        for (Commitment c : commitments) {
            if (c.getIdClient() == clientId) {
                selectedCommitments.add(c);
            }
        }
        String filePath = this.directory + File.separator + clientId + "_commitments.ser";

        FileOutputStream fileOut = new FileOutputStream(filePath);

        ObjectOutputStream objOut = new ObjectOutputStream(fileOut);

        objOut.writeObject(selectedCommitments);
        objOut.flush();

    }

    private void sendCommitmentList(int clientId) throws IOException {
        String filePath = this.directory + File.separator + clientId + "_commitments.ser";

        File file = new File(filePath);

        byte[] fileBytes = new byte[(int) file.length()];

        FileInputStream fileIn = new FileInputStream(file);
        fileIn.read(fileBytes);

        for (byte b : fileBytes) {
            this.out.write(b);
            this.out.flush();
        }
        this.out.write(-1);
        this.out.flush();

        fileIn.close();

    }

    @Override
    public void run() {
        String command;
        try {
            while (true) {
                command = in.readUTF();

                if (command.startsWith("add commitment")) {
                    this.addCommitment(command.split(" ")[2]);
                } else if (command.startsWith("get commitments")) {
                    String clientId = command.split(" ")[2];
                    this.serializeCommitmentList(new Integer(clientId));
                    this.sendCommitmentList(new Integer(clientId));
                } else if (command.equals("exit")) {
                    this.socket.close();
                    this.currentThread().interrupt();
                    if (this.currentThread().isInterrupted()) {
                        break;
                    }
                } else {
                    this.out.writeUTF("Invalid request.");
                    System.out.println("Invalid request.");
                }
            }
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        } catch (ClassNotFoundException cnfe) {
            System.out.println(cnfe.getMessage());
        }
    }

    public static void main(String args[]) {
        int port = 6668;
        commitments = new ArrayList<>();

        try {
            ServerSocket serverSocket = new ServerSocket(port);
            while (true) {

                System.out.println("Waiting for a client...");
                Socket socket = serverSocket.accept();
                Server server = new Server(socket);

                server.start();
                System.out.println("A new client is connected");
            }
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }

    }
}
