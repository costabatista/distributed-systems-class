import commitment_pb2 
import socket
import sys


if __name__ == "__main__":
    # open a socket connection using 6666 port
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    server_address = ("localhost", 6666)
    sock.connect(server_address)

    # Create an object of commitment type
    commitment = commitment_pb2.Commitment()
    commitment.id = 1
    commitment.description = "protobuf make things better"
    commitment.date = "23/04/2018"
    commitment.hour = "23:55"
    # Serializing the object above
    f = open("pythonOutfile", "w")
    f.write(commitment.SerializeToString())

    f.close()

    f = open("pythonOutfile", "rb")

    # sending the serialized file content to Java server.
    byte = f.read()
    sock.sendall(byte)

    # close the opened connection.
    f.close()    
    sock.close()
