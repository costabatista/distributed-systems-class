/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ml.paulobatista.prototask.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import ml.paulobatista.prototask.service.commitment.CommitmentProtos.Commitment;

/**
 *
 * @author costa
 */
public class Server {

    public static void main(String args[]) {
        int port = 6666;
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            System.out.println("Waiting for a client...");
            Socket socket = serverSocket.accept();
            System.out.println("A client is connected.");

            InputStream input = socket.getInputStream();
       
            
            Commitment commitment = Commitment.parseFrom(input);
            
         
            System.out.println("Client id:" + commitment.getId());
            System.out.println("Description: " + commitment.getDescription());
            System.out.println("Date: " + commitment.getDate());
            System.out.println("Hour: " + commitment.getHour());
            
            input.close();
            
            socket.close();
        } catch (IOException ioe) {
            System.out.println("aqui");
            System.out.println(ioe.getMessage());
        }
    }
}
